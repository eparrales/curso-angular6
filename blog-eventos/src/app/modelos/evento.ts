 export class Evento {
  public id: number;
  public organizador: string;
  public nombreEvento: string;
  public lugar: string;
  public fecha: string;
  public hora: string;
  public valor: number;
  public estado: string;

   // tslint:disable-next-line:max-line-length
  constructor(id: number, organizador: string, nombreEvento: string, lugar: string, fecha: string, hora: string, valor: number, estado: string) {
    this.id = id;
    this.organizador = organizador;
    this.nombreEvento = nombreEvento;
    this.lugar = lugar;
    this.fecha = fecha;
    this.hora = hora;
    this.valor = valor;
    this.estado = estado;
  }
}
