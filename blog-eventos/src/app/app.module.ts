import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EventoComponent } from './evento/evento.component';
import { ListaEventosComponent } from './lista-eventos/lista-eventos.component';

@NgModule({
  declarations: [
    AppComponent,
    EventoComponent,
    ListaEventosComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
