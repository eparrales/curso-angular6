import { Component, OnInit } from '@angular/core';
import { Evento } from '../modelos/evento';

@Component({
  selector: 'app-evento',
  templateUrl: './evento.component.html',
  styleUrls: ['./evento.component.css']
})
export class EventoComponent implements OnInit {
  evento: Evento;
  eventos: Evento[];
  idAux: number;
  estado: string;
  constructor() {this.idAux = 0; this.estado = 'Activo'; this.eventos = []; }

  ngOnInit(): void {
  }

  registrarEvento(nombre: string, organizador: string, lugar: string, fecha: string, hora: string, valor: string): boolean{
   this.idAux = this.getIdEvento(this.eventos);
   this.eventos.push(new Evento(this.idAux, organizador, nombre, lugar, fecha, hora, parseInt(valor, 10), this.estado));
   console.log('Eventos:', this.eventos);
   return false;

  }
  getIdEvento(eventos: Evento[]): number{
    this.idAux = this.idAux + 1;
    return this.idAux;
  }
}
