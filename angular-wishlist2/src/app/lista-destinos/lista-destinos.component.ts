import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.models';
import { DestinosApiClient } from '../models/destinos-api-client';
import {AppState} from '../app.module';
import {Store} from '@ngrx/store';
import {ElegidoFavoritoAction, NuevoDestinoAction} from '../models/destinos-viajes-state.model';



@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  // tslint:disable-next-line:no-output-on-prefix
  // @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  // destinos: DestinoViaje[];
  // destinosApiClient: DestinosApiClient;


  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    // this.destinos = [];
    // this.onItemAdded = new EventEmitter<DestinoViaje>();
    this.updates = [];

    this.destinosApiClient.subscribeOnChage((d: DestinoViaje) => {
      if (d != null){
        this.updates.push('Se ha eligido' + d.nombre);
      }
    });
    // this.store.select(estate => estate.destinos.favorito)
    // .subscribe(d => {
    //   if ( d != null ){
    //     this.updates.push('Se ha eligido a' + d.nombre);
    //   }
    // });
  }

  ngOnInit(): void {
  }

 agregado(d: DestinoViaje): void{
    this.destinosApiClient.add(d);
    // this.onItemAdded.emit(d);
   // this.store.dispatch(new NuevoDestinoAction(d));
 }
 elegido(e: DestinoViaje): void{
    // this.destinos.forEach(x => x.setSelected(false));
   // d.setSelected(true);
    this.destinosApiClient.elegir(e);
   // this.store.dispatch(new ElegidoFavoritoAction(e));
 }
}
