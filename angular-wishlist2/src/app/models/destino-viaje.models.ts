export class DestinoViaje {
  // tslint:disable-next-line:variable-name
  private selected: boolean;
  public servicios: string[];
  id: any;
  constructor(public nombre: string, public url: string) {
    this.servicios = ['pileta' , 'desayuno'];
  }

   isSelected(): boolean {
    return this.selected;
  }

    setSelected(value: boolean): void {
    this.selected = value;
  }
}
