import {DestinoViaje} from './destino-viaje.models';
import {BehaviorSubject, Subject} from 'rxjs';
import {Injectable} from '@angular/core';
@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[];
  current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
  constructor() {
    this.destinos = [];
  }
  add(d: DestinoViaje): void{
    this.destinos.push(d);
  }
  getAll(): DestinoViaje[] {
    return  this.destinos;
  }
  getById(id: string): DestinoViaje{
    // tslint:disable-next-line:only-arrow-functions typedef
    return  this.destinos.filter(function( d){
      return d.id.toString() === id;
    })[0];
  }
  elegir(d: DestinoViaje): void{
    this.destinos.forEach(x => x.setSelected(false));
    d.setSelected(true);
    this.current.next(d);
  }
  subscribeOnChage(fn): void {
    this.current.subscribe(fn);
  }
}
