import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  nombre:string
  @Input() listDestinos:string[];
  constructor() {
    this.nombre="Edwin parrales";
   }

  ngOnInit(): void {
  }

}
