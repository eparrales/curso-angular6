import {DestinoViaje} from './destino-viaje.models';
import {BehaviorSubject, Subject} from 'rxjs';
import {Injectable} from '@angular/core';
import {AppState} from '../app.module';
import {Action, Store} from '@ngrx/store';
import {ElegidoFavoritoAction, NuevoDestinoAction, VoteResetAction} from './destinos-viajes-state.model';
@Injectable()
export class DestinosApiClient {

  constructor(private store: Store<AppState>) {

  }
  add(d: DestinoViaje): void{
    this.store.dispatch(new NuevoDestinoAction(d));
  }


  elegir(d: DestinoViaje): void{
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
  resetVote(): void{
    this.store.dispatch( new VoteResetAction());
  }
}
