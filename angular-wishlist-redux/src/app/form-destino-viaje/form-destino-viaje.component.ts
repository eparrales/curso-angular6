import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DestinoViaje} from '../models/destino-viaje.models';
import {FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {fromEvent} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map, switchMap} from 'rxjs/operators';
import {ajax} from 'rxjs/ajax';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  // tslint:disable-next-line:no-output-on-prefix
 @Output() onItemAdded: EventEmitter<DestinoViaje>;
 fg: FormGroup;
 minLongittud = 3;
 searchResults: string[];
  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParmetrizable(this.minLongittud)
      ])],
      url: ['']
    });
    this.fg.valueChanges.subscribe((form: any) => {
        console.log('Cambio form', form);
      }
    );
  }

  ngOnInit(): void {
    const elemNombre = document.getElementById('nombre') as HTMLInputElement as HTMLInputElement;
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value ),
        filter(x => x.length > 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(() => ajax('/assets/datos.json'))
      ).subscribe(ajaxResponse => {
        this.searchResults = ajaxResponse.response;
    });

  }
 guardar(nombre: string, url: string): boolean {
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
 }
 nombreValidator(control: FormControl): {[s: string]: boolean}{
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5){
      return {invalidNombre: true};
    }
    return  null;
 }
  // Validador parametrizable
 nombreValidatorParmetrizable(minLong: number): ValidatorFn{
    return (control: FormControl): {[s: string]: boolean} | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong){
        return {minLongNombre: true};
      }
      return  null;
    };
 }



}
