import {Component, OnInit, Input, HostBinding, Output, EventEmitter} from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.models';
import {AppState} from '../app.module';
import {Store} from '@ngrx/store';
import {VoteDownAction, VoteUpAction} from '../models/destinos-viajes-state.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
 @Input() destino: DestinoViaje;
 // @Input() position: number;
  // tslint:disable-next-line:no-input-rename
 @Input('idx') position: number;
 @HostBinding('attr.class') cssClass = 'col-md-4';
 @Output() clicked: EventEmitter<DestinoViaje>;
  constructor( private store: Store<AppState>) {
     this.clicked = new EventEmitter<DestinoViaje>();
   }

  ngOnInit(): void {
  }
  ir(): boolean {
    this.clicked.emit(this.destino);
    return false;
  }
  voteUp(): boolean{
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }
  voteDown(): boolean{
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
}
