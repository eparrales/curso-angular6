import {Directive, OnDestroy, OnInit} from '@angular/core';

@Directive({
  selector: '[appEspiame]'
})
export class EspiameDirective implements OnInit, OnDestroy{
  static  nextId = 0;
  loger = (msg: string) => console.log(`Evento ${++EspiameDirective.nextId} ${msg}`);
  constructor() { }
  ngOnInit(): void {
     this.loger('##########on init');
  }
  ngOnDestroy(): void {
    this.loger('##########on Destroy');
  }

}
