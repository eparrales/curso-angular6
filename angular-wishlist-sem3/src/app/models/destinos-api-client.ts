import {DestinoViaje} from './destino-viaje.models';
import {BehaviorSubject, Subject} from 'rxjs';
import {forwardRef, Inject, Injectable} from '@angular/core';
import {APP_CONFIG, AppConfig, AppState, db} from '../app.module';
import {Action, Store} from '@ngrx/store';
import {ElegidoFavoritoAction, NuevoDestinoAction, VoteResetAction} from './destinos-viajes-state.model';
import {HttpClient, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[] = [];
  constructor(
    private store: Store<AppState>,
    @Inject( forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient) {

  }
  /*add(d: DestinoViaje): void{
    this.store.dispatch(new NuevoDestinoAction(d));
  }*/

  add(d: DestinoViaje): void {
    const header: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndPoint + '/my', { nuevo: d.nombre }, { headers: header });
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoDestinoAction(d));
        const myDb = db;
        myDb.destinos.add(d);
        console.log('todos los destinos de la db!');
        myDb.destinos.toArray().then(destinos => console.log(destinos));
      }
    });
  }



  elegir(d: DestinoViaje): void{
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
  resetVote(): void{
    this.store.dispatch( new VoteResetAction());
  }
  getById(id: string): DestinoViaje {
        this.store.select(state => state.destinos.items).subscribe(items => this.destinos = items);
        console.log('LLAmado', this.destinos);
        return this.destinos.filter(x => x.id.toString() === id)[0];
  }
}
