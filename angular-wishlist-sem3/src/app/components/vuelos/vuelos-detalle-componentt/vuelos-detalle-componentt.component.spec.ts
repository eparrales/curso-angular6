import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VuelosDetalleComponenttComponent } from './vuelos-detalle-componentt.component';

describe('VuelosDetalleComponenttComponent', () => {
  let component: VuelosDetalleComponenttComponent;
  let fixture: ComponentFixture<VuelosDetalleComponenttComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VuelosDetalleComponenttComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VuelosDetalleComponenttComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
