import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.models';
import { DestinosApiClient } from '../../models/destinos-api-client';
import {AppState} from '../../app.module';
import {Store} from '@ngrx/store';
import {ElegidoFavoritoAction, NuevoDestinoAction} from '../../models/destinos-viajes-state.model';



@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinosApiClient ]
})
export class ListaDestinosComponent implements OnInit {
  // tslint:disable-next-line:no-output-on-prefix
  // @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {

    this.updates = [];

    this.store.select(estate => estate.destinos.favorito)
    .subscribe(d => {
      if ( d != null ){
        this.updates.push('Se ha eligido a' + d.nombre);
      }
    });
    store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
  }

 agregado(d: DestinoViaje): void{
   this.destinosApiClient.add(d);
 }
 elegido(e: DestinoViaje): void{
   this.destinosApiClient.elegir(e);
 }

 reset(): boolean{
     this.destinosApiClient.resetVote();
     return false;
 }

}
